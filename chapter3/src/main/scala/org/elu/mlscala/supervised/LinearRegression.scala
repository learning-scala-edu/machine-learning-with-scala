package org.elu.mlscala.supervised

import breeze.linalg.{*, DenseMatrix, DenseVector, inv}
import breeze.stats.mean

/**
  * Created by luhtonen on 10.05.17.
  */
class LinearRegression(inputs: DenseMatrix[Double],
                       outputs: DenseMatrix[Double],
                       basisFn: Option[DenseVector[Double] => DenseVector[Double]] = None) {

  val x: DenseMatrix[Double] = basisFn match {
    case Some(bf) => inputs(*, ::).map(dv => bf(dv))
    case None => inputs
  }

  def predict(weights: DenseMatrix[Double],
              input: DenseMatrix[Double]): DenseMatrix[Double] = {
    input * weights
  }

  def train(inputs: DenseMatrix[Double] = x,
            outputs: DenseMatrix[Double] = outputs,
            regularizationParam: Double = 0.0): DenseMatrix[Double] = {
    val l = inputs.cols

    val identMat = DenseMatrix.eye[Double](l)
    val regPenalty = regularizationParam * l

    inv(inputs.t * inputs + regPenalty * identMat) * (inputs.t * outputs)
  }

  def evaluate(weights: DenseMatrix[Double],
               inputs: DenseMatrix[Double],
               targets: DenseMatrix[Double],
               evaluator: (DenseMatrix[Double], DenseMatrix[Double]) => Double): Double = {
    val preds = predict(weights, inputs)
    evaluator(preds, targets)
  }

  def crossValidation(folds: Int,
                      regularizationParam: Double,
                      evaluator: (DenseMatrix[Double], DenseMatrix[Double]) => Double): Double = {

    val foldSize = x.rows / folds.toDouble

    //segment dataset
    val partitions = (0 until x.rows).grouped(math.ceil(foldSize).toInt)

    val ptSet = (0 until x.rows).toSet

    //compute test error for each fold
    val xValError = partitions.foldRight(Vector.empty[Double]) { (c, acc) =>

      //training data points are all data points not in validation set.
      val trainIdx = ptSet.diff(c.toSet)
      val testIdx = c

      //training data
      val trainX = x(trainIdx.toIndexedSeq, ::).toDenseMatrix
      val trainY = outputs(trainIdx.toIndexedSeq, ::).toDenseMatrix

      //test data
      val testX = x(testIdx, ::).toDenseMatrix
      val testY = outputs(testIdx, ::).toDenseMatrix

      //train a weight vector with the above training data
      val weights = train(trainX, trainY, regularizationParam)

      //compute the error on the held-out test data
      val error = evaluate(weights, testX, testY, evaluator)

      //append error to the accumulator so it can be average later
      acc :+ error
    }

    mean(xValError)
  }
}
