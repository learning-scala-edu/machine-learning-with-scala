package org.elu.mlscala.supervised

import breeze.linalg.{*, DenseMatrix, DenseVector}

/**
  * Created by luhtonen on 11.05.17.
  */
class NearestNeighbours(k: Int,
                        dataX: DenseMatrix[Double],
                        dataY: Seq[String],
                        distanceFn: (DenseVector[Double], DenseVector[Double]) => Double) {

  def predict(x: DenseVector[Double]): String = {
    val distances = dataX(*, ::).map(r => distanceFn(r, x))

    val topKClasses = distances
      .toArray
      .zipWithIndex
      .sortBy(_._1)
      .take(k)
      .map { case (dist, idx) => dataY(idx) }

    topKClasses
      .groupBy(identity)
      .mapValues(_.length)
      .maxBy(_._2)._1
  }
}
