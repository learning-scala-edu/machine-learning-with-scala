package org.elu.mlscala.neuralnetworks

import breeze.linalg.DenseVector

/**
  * Created by luhtonen on 19.05.17.
  */
object RBMExample extends App {

  val dataset = Seq(
    DenseVector(1.0, 1.0, 1.0, 0.0, 0.0, 0.0),
    DenseVector(1.0, 0.0, 1.0, 0.0, 0.0, 0.0),
    DenseVector(1.0, 1.0, 1.0, 0.0, 0.0, 0.0),
    DenseVector(0.0, 0.0, 1.0, 1.0, 1.0, 0.0),
    DenseVector(0.0, 0.0, 1.0, 1.0, 0.0, 0.0),
    DenseVector(0.0, 0.0, 1.0, 1.0, 1.0, 0.0)
  )

  RestrictedBoltzmannMachine.learn(data = dataset,
    numHiddenUnits = 2,
    batchSize = 6,
    initialLearningRate = 0.05)
}
