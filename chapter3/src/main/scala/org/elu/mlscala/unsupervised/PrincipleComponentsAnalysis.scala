package org.elu.mlscala.unsupervised

import breeze.linalg.{*, DenseMatrix, DenseVector, PCA}
import breeze.stats.mean

/**
  * Created by luhtonen on 19.05.17.
  */
class PrincipleComponentsAnalysis(data: DenseMatrix[Double]) {
  val covMat: DenseMatrix[Double] = {
    val empMean: DenseMatrix[Double] = mean(data(::, *)).t.asDenseMatrix

    var covariance = DenseMatrix.zeros[Double](data.cols, data.cols)

    (0 until data.rows).foreach { dpId =>

      val dp = data(dpId, ::).t

      val dpMinusMu = dp - empMean.toDenseVector

      covariance += dpMinusMu * dpMinusMu.t
    }
    covariance.map(_ / (data.rows - 1))
  }

  val pca = new PCA(data, covMat)

  val componentVariance: DenseVector[Double] = pca.propvar

  val transformedData: DenseMatrix[Double] = pca.scores
}
