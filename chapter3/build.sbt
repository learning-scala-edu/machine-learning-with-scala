name := "chapter3"

version := "1.0"

scalaVersion := "2.11.11"

libraryDependencies  ++= Seq(
  "org.scalanlp" %% "breeze" % "0.13.1",
  "org.scalanlp" %% "breeze-natives" % "0.13.1",
  "org.scalanlp" %% "breeze-viz" % "0.13.1",
  "com.typesafe.akka" %% "akka-actor" % "2.5.1",
  "com.quantifind" %% "wisp" % "0.0.4",
  "org.scala-saddle" %% "saddle-core" % "1.3.4",
  "com.sparkjava" % "spark-core" % "2.6.0",
  "org.apache.spark" %% "spark-mllib" % "2.1.1"
)
