name := "breeze-example"

version := "1.0"

scalaVersion := "2.11.11"

libraryDependencies  ++= Seq(
  "org.scalanlp" %% "breeze" % "0.13.1",
  "org.scalanlp" %% "breeze-natives" % "0.13.1",
  "org.scalanlp" %% "breeze-viz" % "0.13.1",
  "com.quantifind" %% "wisp" % "0.0.4"
)
resolvers += "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
